package com.eyalya94.fido.test.common

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.eyalya94.fido.test.repos.abs.BaseRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import retrofit2.Response


fun ViewModel.runCoroutine(block: suspend CoroutineScope.() -> Unit): Job {
    return viewModelScope.launch {
        block()
    }
}

inline fun <T> BaseRepository.safeApiCall(apiCall: () -> Response<T>): SimpleResponse<T> {
    return try {
        SimpleResponse.success(apiCall.invoke())
    } catch (e: Exception) {
        SimpleResponse.failure(e)
    }
}