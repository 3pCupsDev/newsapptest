package com.eyalya94.fido.test.model

import com.google.gson.annotations.SerializedName

class NewsListResponse(
    val status: String,
    val totalResults: Int,
    val articles: ArrayList<Article>
)

class NewsSource(
    val id: String,
    val name: String
)

class Article(
    val source: NewsSource,
    val author: String,
    val title: String,
    val description : String,
    val url: String,
    @SerializedName("urlToImage")
    val imageUrl: String
)



sealed class ErrorEvent(val msg: String = "") {
    object NetworkError : ErrorEvent("Network error occurred")
    object GeneralError : ErrorEvent("General Error!!")
    object NoErrors : ErrorEvent()
}