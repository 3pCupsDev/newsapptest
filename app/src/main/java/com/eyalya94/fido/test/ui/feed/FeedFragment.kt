package com.eyalya94.fido.test.ui.feed

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.eyalya94.fido.test.adapter.NewsFeedAdapter
import com.eyalya94.fido.test.common.BaseFragment
import com.eyalya94.fido.test.databinding.FeedFragmentBinding
import com.eyalya94.fido.test.model.ErrorEvent
import com.eyalya94.fido.test.model.NewsFeedData
import com.google.android.material.snackbar.Snackbar

class FeedFragment : BaseFragment<FeedFragmentBinding>() {

    private lateinit var feedAdapter: NewsFeedAdapter
    override lateinit var binding: FeedFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        if (!::binding.isInitialized) {
            binding = FeedFragmentBinding.inflate(inflater, container, false)
        }
        return binding.root
    }

    override fun initUi() {
        binding.apply {
            newsfeedRecycler.apply {
                layoutManager = LinearLayoutManager(requireContext())
                feedAdapter = NewsFeedAdapter { newsItem, extras ->
                    Log.d("wow", "on News feed item clicked: ${newsItem.title} ")
                    mainViewModel.openArticle(newsItem, extras)
                }
                adapter = feedAdapter
            }
        }
    }

    override fun initObservers() = with(mainViewModel) {
        errorEventData.observe(viewLifecycleOwner) { errorEvent ->
            handleErrors(errorEvent)
        }
        newsFeedLiveData.observe(viewLifecycleOwner) { feed->
            handleNewsFeed(feed)
        }
    }

    private fun handleNewsFeed(feed: NewsFeedData?) {
        feed?.apply {
            if (articles?.isNullOrEmpty() == false) {
                feedAdapter.submitList(articles)
            }
        }
    }

    private fun handleErrors(errorEvent: ErrorEvent?) {
        when (errorEvent) {
            is ErrorEvent.GeneralError -> {
                Toast.makeText(
                    requireContext(),
                    errorEvent.msg,
                    Toast.LENGTH_SHORT
                ).show()
            }
            is ErrorEvent.NetworkError -> {
                Snackbar.make(
                    binding.root,
                    errorEvent.msg,
                    Snackbar.LENGTH_INDEFINITE
                ).show()
            }
            is ErrorEvent.NoErrors -> {}
            else -> {}
        }
    }


}