package com.eyalya94.fido.test.repos.abs

import com.eyalya94.fido.test.common.Constants
import com.eyalya94.fido.test.common.safeApiCall
import com.eyalya94.fido.test.model.Article
import com.eyalya94.fido.test.model.NewsFeedData
import com.eyalya94.fido.test.network.ApiService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

abstract class SimpleNewsDataFetcher(private val apiService: ApiService, private val sources: String) :
    NewsDataFetcher {

    override suspend fun fetchNewsFeed(): NewsFeedData {
        var articles: ArrayList<Article>? = arrayListOf<Article>()
        val job = CoroutineScope(Dispatchers.IO).launch {
            val response = safeApiCall { apiService.getNewsListBySource(sources) }
            if (response.isSuccessful && response.body.articles.isNotEmpty() && response.body.status == Constants.API_RESPONSE_STATUS_OK) {
                articles?.addAll(response.body.articles)
            } else {
                articles = null
            }
        }
        job.join()

        return NewsFeedData(articles)
    }
}