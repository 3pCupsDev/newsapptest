package com.eyalya94.fido.test

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.navigation.fragment.FragmentNavigator
//import androidx.navigation.fragment.FragmentNavigator
import com.eyalya94.fido.test.common.ProgressData
import com.eyalya94.fido.test.common.runCoroutine
import com.eyalya94.fido.test.model.Article
import com.eyalya94.fido.test.model.ErrorEvent
import com.eyalya94.fido.test.model.NavigationEvents
import com.eyalya94.fido.test.model.NewsFeedData
import com.eyalya94.fido.test.repos.abs.NavigationManager
import com.eyalya94.fido.test.repos.abs.NewsDataFetcher

class MainViewModel(private val newsDataFetcher: NewsDataFetcher, private val navigationManager: NavigationManager): ViewModel() {

    val progressData = ProgressData()
    val errorEventData = MutableLiveData<ErrorEvent>()
    val currentNavigationData: LiveData<NavigationEvents> = navigationManager.currentNavigationData

    val newsFeedLiveData: MutableLiveData<NewsFeedData> = MutableLiveData<NewsFeedData>()

    fun loadNewsFeed() {
        progressData.startProgress()

        runCoroutine {
            val newsFeedData = newsDataFetcher.fetchNewsFeed()
            when {
                !newsFeedData.articles.isNullOrEmpty() -> {
                    newsFeedLiveData.postValue(newsFeedData)
                    errorEventData.postValue(ErrorEvent.NoErrors)
                }
                newsFeedData.articles.isNullOrEmpty() -> {
                    errorEventData.postValue(ErrorEvent.NetworkError)
                }
                else -> {
                    errorEventData.postValue(ErrorEvent.GeneralError)
                }
            }
            progressData.endProgress()
        }
    }

    fun openArticle(article: Article, extras: FragmentNavigator.Extras? = null) = with(navigationManager) {
        progressData.startProgress()
        goToArticle(article, extras)
    }

    fun goBack() = with(navigationManager) {
        progressData.startProgress()
        goToHome()
    }

}