package com.eyalya94.fido.test.repos.abs

import androidx.lifecycle.MutableLiveData
import androidx.navigation.fragment.FragmentNavigator
import com.eyalya94.fido.test.model.Article
import com.eyalya94.fido.test.model.NavigationEvents

interface NavigationManager : BaseRepository {

    val currentNavigationData: MutableLiveData<NavigationEvents>

    fun goToHome()
    fun goToArticle(article: Article, extras: FragmentNavigator.Extras? = null)
}

