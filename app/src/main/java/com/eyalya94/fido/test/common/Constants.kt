package com.eyalya94.fido.test.common

object Constants {
    const val API_RESPONSE_STATUS_OK = "ok"
    const val API_KEY = "b1a6e332ba964dcdb602907a74ae7b02"
    const val API_BASE_URL = "https://newsapi.org/v2/"
    const val API_GET_ENDPOINT_SUFFIX = "top-headlines/"
}