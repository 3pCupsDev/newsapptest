package com.eyalya94.fido.test.ui.article

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import coil.load
import com.eyalya94.fido.test.common.BaseFragment
import com.eyalya94.fido.test.databinding.ArticleFragmentBinding
import com.eyalya94.fido.test.model.Article
import com.eyalya94.fido.test.model.NavigationEvents

class ArticleFragment : BaseFragment<ArticleFragmentBinding>() {

    override lateinit var binding: ArticleFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        if (!::binding.isInitialized) {
            binding = ArticleFragmentBinding.inflate(inflater, container, false)
        }
        return binding.root
    }

    override fun initUi() {}

    override fun initObservers() = with(mainViewModel) {
        currentNavigationData.observe(viewLifecycleOwner) {
            if(it is NavigationEvents.ShowArticleScreen) {
                bindArticle(it)
            }
        }
    }

    private fun bindArticle(articleScreen: NavigationEvents.ShowArticleScreen) = with(binding) {
        articleScreen.article.apply {
            articleFragTopImage.load(imageUrl)
            animateUi(this)

            articleFragTitle.text = title
            articleFragDescription.text = description
        }
    }

    private fun animateUi(article: Article) = with(binding) {
        articleFragTitle.transitionName = article.title + "title"
        articleFragDescription.transitionName = article.description + "desc"
        articleFragTopImage.transitionName = article.imageUrl + "image"
    }
}