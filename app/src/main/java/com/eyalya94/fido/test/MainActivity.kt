package com.eyalya94.fido.test

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.view.isVisible
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.Navigation.findNavController
import com.eyalya94.fido.test.databinding.MainActivityBinding
import com.eyalya94.fido.test.model.NavigationEvents
import org.koin.androidx.viewmodel.ext.android.viewModel
import kotlin.random.Random

class MainActivity : AppCompatActivity(), NavController.OnDestinationChangedListener {

    private val mainViewModel by viewModel<MainViewModel>()

    private lateinit var binding: MainActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = MainActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initObservers()
    }

    private fun initObservers() = with(mainViewModel) {
        currentNavigationData.observe(this@MainActivity) { navigationEvent ->
            handleNavigationEvents(navigationEvent)
        }
        progressData.observe(this@MainActivity) { isLoading ->
            handleLoading(isLoading)
        }
    }

    private fun handleNavigationEvents(navigationEvent: NavigationEvents?) {
        navigationEvent?.let {
            findNavController(binding.fragContainerView)
                .let { navController: NavController ->
                when (it) {
                    is NavigationEvents.ShowArticleScreen -> {
                        navController.navigate(
                            R.id.ArticleFragment, null,
                            null, it.extras
                        )
                    }
                    is NavigationEvents.ShowHomeScreen -> {
                        navController.navigate(R.id.FeedFragment)
                    }
                }
            }
        }
    }

    private fun handleLoading(isLoading: Boolean) = with(binding) {
        progressBar.isVisible = isLoading
    }

    override fun onStart() {
        super.onStart()
        findNavController(binding.fragContainerView).addOnDestinationChangedListener(this)
    }

    override fun onResume() {
        super.onResume()
        mainViewModel.loadNewsFeed()
    }

    override fun onStop() {
        super.onStop()
        findNavController(binding.fragContainerView).removeOnDestinationChangedListener(this)
    }

    override fun onBackPressed() {
        if (mainViewModel.currentNavigationData.value is NavigationEvents.ShowArticleScreen) {
            mainViewModel.goBack()
        } else {
            finish()
        }
    }

    override fun onDestinationChanged(
        controller: NavController,
        destination: NavDestination,
        arguments: Bundle?
    ) {
        when (destination.id) {
            R.id.FeedFragment -> {
                addFakeProgressTransition()
            }
            R.id.ArticleFragment -> {
                addFakeProgressTransition()
            }
        }
    }

    private fun addFakeProgressTransition() {
        binding.root.postDelayed({
            handleLoading(false)
        }, Random.nextLong(220, 666))
    }
}