package com.eyalya94.fido.test

import android.app.Application
import com.eyalya94.fido.test.di.networkModule
import com.eyalya94.fido.test.di.appModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.GlobalContext.startKoin
import org.koin.core.logger.Level

class NewsApp : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger(Level.ERROR)
            androidContext(this@NewsApp)
            modules(appModule, networkModule)
        }
    }

}