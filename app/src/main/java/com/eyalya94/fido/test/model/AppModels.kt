package com.eyalya94.fido.test.model

import androidx.navigation.fragment.FragmentNavigator

sealed class NavigationEvents {
    class ShowHomeScreen() : NavigationEvents()
    class ShowArticleScreen(val article: Article, val extras: FragmentNavigator.Extras? = null) : NavigationEvents()
}

class NewsFeedData(
    val articles: ArrayList<Article>?
)



