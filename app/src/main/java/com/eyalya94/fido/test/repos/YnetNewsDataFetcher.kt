package com.eyalya94.fido.test.repos

import com.eyalya94.fido.test.network.ApiService
import com.eyalya94.fido.test.repos.abs.SimpleNewsDataFetcher

class YnetNewsDataFetcher(apiService: ApiService) : SimpleNewsDataFetcher(apiService, "ynet") {
}