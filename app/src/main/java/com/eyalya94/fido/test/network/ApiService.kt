package com.eyalya94.fido.test.network

import com.eyalya94.fido.test.common.Constants
import com.eyalya94.fido.test.model.NewsListResponse
import retrofit2.http.GET
import retrofit2.Response
import retrofit2.http.Query

interface ApiService {
    @GET(Constants.API_GET_ENDPOINT_SUFFIX)
    suspend fun getNewsListBySource(
        @Query("sources") source: String,
        @Query("apiKey") key: String = Constants.API_KEY
    ): Response<NewsListResponse>
}