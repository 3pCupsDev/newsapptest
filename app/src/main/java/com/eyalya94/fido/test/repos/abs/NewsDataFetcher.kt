package com.eyalya94.fido.test.repos.abs

import com.eyalya94.fido.test.model.Article
import com.eyalya94.fido.test.model.NewsFeedData

interface NewsDataFetcher : BaseRepository {

    suspend fun fetchNewsFeed() : NewsFeedData

}