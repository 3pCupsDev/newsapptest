package com.eyalya94.fido.test.repos

import com.eyalya94.fido.test.network.ApiService
import com.eyalya94.fido.test.repos.abs.SimpleNewsDataFetcher

class WallaNewsDataFetcher(apiService: ApiService) : SimpleNewsDataFetcher(apiService, "walla") {
}