package com.eyalya94.fido.test.di

import com.eyalya94.fido.test.MainViewModel
import com.eyalya94.fido.test.network.ApiService
import com.eyalya94.fido.test.repos.NavigationManagerImpl
import com.eyalya94.fido.test.repos.WallaNewsDataFetcher
import com.eyalya94.fido.test.repos.YnetNewsDataFetcher
import com.eyalya94.fido.test.repos.abs.NavigationManager
import com.eyalya94.fido.test.repos.abs.NewsDataFetcher
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {

    single { getYnetNewsDataFetcher(get()) }
    single { getWallaNewsDataFetcher(get()) }

    single { getNavigationManager() }


    viewModel { MainViewModel(get(), get()) }
}

fun getYnetNewsDataFetcher(apiService: ApiService): NewsDataFetcher = YnetNewsDataFetcher(apiService)
fun getWallaNewsDataFetcher(apiService: ApiService): NewsDataFetcher = WallaNewsDataFetcher(apiService)

fun getNavigationManager() : NavigationManager = NavigationManagerImpl()