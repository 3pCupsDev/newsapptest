package com.eyalya94.fido.test.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.fragment.FragmentNavigator
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.eyalya94.fido.test.R
import com.eyalya94.fido.test.model.Article
import com.hardik.clickshrinkeffect.applyClickShrink

class NewsFeedAdapter(private val items: ArrayList<Article> = arrayListOf(), val onNewsItemClick: (Article, FragmentNavigator.Extras) -> Unit) : RecyclerView.Adapter<NewsFeedAdapter.NewsItemViewHolder>() {

    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsItemViewHolder =
        NewsItemViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.news_item, parent, false))

    override fun onBindViewHolder(holder: NewsItemViewHolder, position: Int) {
        items[position].apply {
            holder.bind(this)
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    fun submitList(newFeed: ArrayList<Article>) {
        items.clear()
        items.addAll(newFeed)
        notifyDataSetChanged()
    }

    inner class NewsItemViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {

        private val image: ImageView = view.findViewById(R.id.article_image)
        private val title: TextView = view.findViewById(R.id.article_title)
        private val desc: TextView = view.findViewById(R.id.article_desc)

        init {
            view.applyClickShrink()
        }

        fun bind(newsItem: Article) {
            image.load(newsItem.imageUrl)
            title.text = newsItem.title
            desc.text = newsItem.description

            view.setOnClickListener {
                title.transitionName = newsItem.title + "title"
                desc.transitionName = newsItem.description + "desc"
                image.transitionName = newsItem.imageUrl + "image"
                val extras = FragmentNavigatorExtras(
                    title to title.transitionName,
                    desc to desc.transitionName,
                    image to image.transitionName
                )

                onNewsItemClick.invoke(newsItem, extras)
            }
        }
    }

}