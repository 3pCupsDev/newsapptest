package com.eyalya94.fido.test.repos

import androidx.lifecycle.MutableLiveData
import androidx.navigation.fragment.FragmentNavigator
import com.eyalya94.fido.test.model.Article
import com.eyalya94.fido.test.model.NavigationEvents
import com.eyalya94.fido.test.repos.abs.NavigationManager

class NavigationManagerImpl : NavigationManager {

    override val currentNavigationData = MutableLiveData<NavigationEvents>()

    init {
        goToHome()
    }

    override fun goToHome() = currentNavigationData.postValue(NavigationEvents.ShowHomeScreen())

    override fun goToArticle(article: Article, extras: FragmentNavigator.Extras?) =
        currentNavigationData.postValue(NavigationEvents.ShowArticleScreen(article, extras))
}