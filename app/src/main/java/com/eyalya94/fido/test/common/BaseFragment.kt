package com.eyalya94.fido.test.common

import android.os.Bundle
import android.transition.TransitionInflater
import android.view.View
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import com.eyalya94.fido.test.MainViewModel
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

abstract class BaseFragment<VB : ViewBinding> : Fragment() {

    abstract var binding: VB

    protected val mainViewModel by sharedViewModel<MainViewModel>()

    abstract fun initObservers()
    abstract fun initUi()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val moveTransition =
            TransitionInflater.from(context).inflateTransition(android.R.transition.move)
        val fadeTransition =
            TransitionInflater.from(context).inflateTransition(android.R.transition.fade)

        sharedElementEnterTransition = moveTransition
        sharedElementReturnTransition = moveTransition

        enterTransition = fadeTransition
        exitTransition = fadeTransition
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initObservers()
        initUi()
    }

}