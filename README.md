# **News App**
# README #
This app is a test project for Fido by Eyal Yaakobi

# Used Libraries #
In this project I've used the following libs:<br/>
* [Koin](https://github.com/InsertKoinIO/koin) - for Dependency Injection<br/>
* [Coil](https://github.com/coil-kt/coil) - for fast image requests using coroutines<br/>
* [Retrofit](https://github.com/square/retrofit), [OkHttp](https://github.com/square/okhttp) and [Gson](https://github.com/google/gson) - for networking<br/>
* [Nav Graph](https://developer.android.com/guide/navigation/navigation-getting-started) - for easy fragment transitions with shared element animations<br/>
* [Shared Element Transitions](https://developer.android.com/guide/fragments/animate) - for beautiful fragment transitions<br/>
* [RecyclerView](https://developer.android.com/jetpack/androidx/releases/recyclerview) - For news feed <br/>
* [Coroutines](https://developer.android.com/kotlin/coroutines) - Make asynchronously calls with coroutines scopes<br/><br/>

[Click here to see more cool code repos by Eyal Yaakobi](https://bitbucket.org/3pCupsDev/)<br/>
[Another link](https://github.com/unix14/)

# Code Structure #
The architecture i've used is MVVM, and the important repositories and classes in this code is as follows:

## NewsDataFetcher ##
### Abstract ###
* **NewsDataFetcher** - used to fetch news list by source from API
### Implementations ###
* **SimpleNewsDataFetcher** - implemented with safe Api call <br/>
* ***YnetNewsDataFetcher*** - For Ynet Articles <br/>
* ***WallaNewsDataFetcher*** - For Walla Articles <br/>

## NavigationManager ##
### Abstract ###
* **NavigationManager** - used to determine which fragment is presented on the screen
### Implementation ###
* **NavigationManagerImpl** - mainly moves article data when clicked from list


## Special Classes ##
* **SimpleResponse** - is used to wrap basic response from retrofit and generalize the response to one object. <br/>
* **ProgressData** - helps to distinct events of loading start and end <br/>
* **BaseFragment** - represents the basic functionalities of each fragment in the app<br/>

## Adapter ##
* **NewsFeedAdapter** - Handles the list for the home fragment newsfeed
[Click here to visit my website](https://3p-cups.com/)